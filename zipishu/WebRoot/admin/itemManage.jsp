<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="utf-8"%>
<%@include file="menu.jsp"%>

<script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.min.js"> </script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>



<jsp:useBean id="db" class="com.zipishu.DataBaseConnection"></jsp:useBean>

<%

	String strCatalogid = request.getParameter("catalogid");
	int catalogid = 0;
	String strCatalogname ="";
	Integer classId = 0;
	String strPhotos="";
	String strAbout = "";

	if(null == strCatalogid || "".equals(strCatalogid)){

	}else{
		catalogid = Integer.parseInt(strCatalogid);
		Connection dbcNews = db.getConnection();
		String sqlNews = "SELECT * FROM catelog where catalogid=?";
		PreparedStatement stmtNews = dbcNews.prepareStatement(sqlNews);
		stmtNews.setInt(1, catalogid);
		ResultSet rsNews = stmtNews.executeQuery();
		if(rsNews.next()){
			strCatalogname = rsNews.getString("catalogname");
			classId = rsNews.getInt("classid");
			strPhotos = rsNews.getString("photos");
			strAbout = rsNews.getString("about");
		}
	}


	Connection dbcClass = db.getConnection();
	Statement stmtClass = dbcClass.createStatement();
	String sqlClass = "SELECT * FROM class";
	ResultSet rsClass = stmtClass.executeQuery(sqlClass);


%>
	<!-- Page Head -->
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3>添加类别</h3>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					<div class="notification information png_bg">
							<a href="#" class="close"><img src="./images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
							<div>
								随便提醒点什么, 注意事项.
							</div>
					</div>
					
					<form id="newsForm" action="itemManage_action.jsp" method="post">
							
							<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
								
								<p>
									<label>名称</label>
									<input class="text-input large-input" type="text" id="catalogname" name="catalogname" value="<%=strCatalogname%>" />
								</p>
								
								<p>
									<label>所属大类</label>              
									<select name="classid" class="small-input">
									<% while(rsClass.next()){ %>
										<option
										<%
										if(rsClass.getInt("classid") == classId ){
											out.print(" selected='selected'");
										}
										%>
										 value="<%=rsClass.getInt("classid")%>"><%=rsClass.getString("classname")%></option>
									<% } %>
									</select> 
								</p>
								
								<p>
									<label>照片</label>
									<input class="text-input large-input" type="text" id="photos" name="photos" value="<%= strPhotos %>" />
								</p>
								
								<p>
									<label>简介</label>
									<!-- <textarea class="text-input textarea wysiwyg" id="content1" name="content1" cols="79" rows="15"></textarea> -->
									
									<script id="content" type="text/plain" style="width:100%;height:200px;"><%=strAbout%></script>

								</p>
								
								<p>
								<%if(catalogid == 0){%>
									<input class="button" type="submit" value="添加类别" />
								<%}else{%>
									<input type="hidden" name="catalogid" value="<%=catalogid%>" />
									<input class="button" type="submit" value="修改类别" />
								<%}%>
								</p>
								
							</fieldset>
							
							<div class="clear"></div><!-- End .clear -->
							
						</form>
					<!-- <div class="notification information png_bg">
						<a href="#" class="close"><img src="./images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
						<div>
							一条信息
						</div>
					</div>
					
					<div class="notification success png_bg">
						<a href="#" class="close"><img src="./images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
						<div>
							成功信息
						</div>
					</div>
					
					<div class="notification attention png_bg">
						<a href="#" class="close"><img src="./images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
						<div>
							警告信息
						</div>
					</div>
					
					<div class="notification error png_bg">
						<a href="#" class="close"><img src="./images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
						<div>
							错误信息
						</div>
					</div> -->
				</div>
			</div>

		<%
		rsClass.close();
		dbcClass.close();
		%>

		<script type="text/javascript">
			
			$(document).ready(function(){
				
				/*如果是没有子菜单的选中，如下
				$('#admin-index').addClass('current');*/
				
				/*如果含有子菜单的，请选中父菜单和子菜单，如下*/
				$('#admin-class').addClass('current');
				$('#admin-class').parent().find("ul").slideToggle("slow");
				$('#admin-class-add').addClass('current');




			    ///实例化编辑器
			    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
			    var ue = UE.getEditor('content');

			    ///ue.setContent("<%=strAbout%>");
				
			});


		</script>



<%@include file="footer.jsp"%>