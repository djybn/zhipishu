<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="utf-8"%>
<%
request.setCharacterEncoding("utf-8");

String strAction = request.getParameter("action");
String strDesid = request.getParameter("desid");



String name = request.getParameter("name");
String address = request.getParameter("address");
String telephone = request.getParameter("telephone");
String avgconsume = request.getParameter("avgconsume");
String strareaid = request.getParameter("areaid");
String strtimeid = request.getParameter("timeid");
String stremotionid = request.getParameter("emotionid");
String strshortid = request.getParameter("shortid");
String parkstate = request.getParameter("parkstate");
String busguide = request.getParameter("busguide");
String recommend = request.getParameter("recommend");
String about = request.getParameter("editorValue");

//判空操作///
if(null == name || "".equals(name)){
	out.println("标题不能为空");
}
////////


com.zipishu.DataBaseConnection db = new com.zipishu.DataBaseConnection();
Connection con = db.getConnection();


String sql = "";

if("delete".equals(strAction)){
	sql = "DELETE FROM destination WHERE desid=?";
	Integer desid = Integer.parseInt(strDesid);
	PreparedStatement pstmt = con.prepareStatement(sql);
	pstmt.setInt(1, desid);
	pstmt.executeUpdate();
}else{

	int areaid = Integer.parseInt(strareaid);
	int timeid = Integer.parseInt(strtimeid);
	int emotionid = Integer.parseInt(stremotionid);
	int shortid = Integer.parseInt(strshortid);
	

	if(null == strDesid || "".equals(strDesid)){
		sql = "INSERT INTO destination(name,address,telephone,avgconsume,areaid,timeid,emotionid,shortid,parkstate,busguide,recommend,about,createdate,updatedate,photos) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,getdate(),getdate(),21312323)";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setString(1, name);
		pstmt.setString(2, address);
		pstmt.setString(3, telephone);
		pstmt.setString(4, avgconsume);
		pstmt.setInt(5, areaid);
		pstmt.setInt(6, timeid);
		pstmt.setInt(7, emotionid);
		pstmt.setInt(8, shortid);
		pstmt.setString(9, parkstate);	
		pstmt.setString(10, busguide);	
		pstmt.setString(11, recommend);	
		pstmt.setString(12, about);	
		pstmt.executeUpdate();
	}else{
		Integer desid = Integer.parseInt(strDesid);
		sql = "UPDATE destination SET name=?,address=?,telephone=?,avgconsume=?,areaid=?,timeid=?,emotionid=?,shortid=?,parkstate=?,busguide=?,recommend=?,about=? WHERE desid=?";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setString(1, name);
		pstmt.setString(2, address);
		pstmt.setString(3, telephone);
		pstmt.setString(4, avgconsume);
		pstmt.setInt(5, areaid);
		pstmt.setInt(6, timeid);
		pstmt.setInt(7, emotionid);
		pstmt.setInt(8, shortid);
		pstmt.setString(9, parkstate);	
		pstmt.setString(10, busguide);	
		pstmt.setString(11, recommend);	
		pstmt.setString(12, about);	
		pstmt.setInt(13, desid);
		pstmt.executeUpdate();
	}
}
con.close();

response.sendRedirect("destinationList.jsp");

%>