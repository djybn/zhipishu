<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<title>紫皮书</title>		
		<!--                       CSS                       -->
	  
		<!-- Reset Stylesheet -->
		<link rel="stylesheet" href="./css/reset.css" type="text/css" media="screen" />
	  
		<!-- Main Stylesheet -->
		<link rel="stylesheet" href="./css/style.css" type="text/css" media="screen" />
		
		<!--                       Javascripts                       -->
  
		<!-- jQuery -->
		<script type="text/javascript" src="./js/jquery-1.3.2.min.js"></script>
		
		<script type="text/javascript" src="./js/zps.js"></script>
		
	</head>
  
	<body><div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->
		
		<div id="header">
			<div id="header-left" class="align-left">
				<div id="logo">
					<h1>紫皮书</h1>
				</div>
			</div>
			<div id="header-right" class="align-right">
				<div id="header-right-up">
					<div id="slogan" class="align-left"><p>精彩你生活</p></div>
					<div id="search-box" class="align-right">
						<span>
							<input type="text" class="input_text_focus" name="pname">
						</span>
						<input name="emotion" type="submit" value="搜索" class="btn_search">
					</div>
				</div>
				<div id="main-menu">
					<ul class="nav">
						<li id="li-index" ><a href="index.jsp">首页</a></li>
						<li id="li-info" class="on"><a href="#">城市情报</a></li>
						<li id="li-map" ><a href="emotion.jsp">情绪地图</a></li>
						<li id="li-shortcut"><a href="shortcut.jsp">休闲快捷</a></li>
						<li id="li-vogue"><a href="#">时尚中人</a></li>
					</ul>
					<!-- End subnav -->
				</div>
			</div>
			<div class="clear"></div>
		</div>	
		
		<div id="main-body">
		<%@include file="left.jsp"%>
		
		<div id="content" class="align-right">
			<div style="padding:10px;">
				<div id="content-ads">
					<div style="padding:0px 0px 10px 0px;"><span><i>◆</i></span>目录：<a href="index.jsp">首页</a>&gt;城市情报</div>
					<img src="images/ads_2.png" width="800" height="90" />
				</div>
				<h2>城市情报</h2>
				<div id="main-content">
					<table>
							<thead>
								<tr>
								   <th>情报标题</th>
								  <th> 副标题</th>
								   <th>来源</th>
								   <th>作者</th>
								   <th> 主要内容</th>
								   <th>来源</th>
								   <th>作者</th>
								</tr>
								
							</thead>
						 
							<tbody>
							
								<% 
									request.setCharacterEncoding("utf-8");
									String s=request.getParameter("title");
									
										Connection dbc = db.getConnection();
										//Statement stmt = dbc.createStatement();
									
										String sql = "select * from news where title=?";
										PreparedStatement pstmt = dbc.prepareStatement(sql);
										pstmt.setString(1, s);
									
										ResultSet rs =  pstmt.executeQuery();

										while(rs.next()){
									%>
										
										<tr class="alt-row">
											<td><%=rs.getString("title") %></td>
											<td><%=rs.getString("subtitle")%></td>
											<td><%=rs.getString("content1")%></td>
											<td><%=rs.getString("source")%></td>
											<td><%=rs.getString("author")%></td>
										
											<td><%=rs.getString("createdate")%></td>
											<td><%=rs.getString("hit")%></td>
										</tr>
										
									<%
										}
										rs.close();
										dbc.close();
									%>
								</tr>
							</div>
								
							</tbody>	
						</table>
						<div style="float:right;margin:20px 60px 0px 0px;">
											
										</div> <!-- End .pagination -->
					</div>
			</div>
			<div class="clear"></div>
		</div>
		
		<%@include file="bottom.jsp"%>